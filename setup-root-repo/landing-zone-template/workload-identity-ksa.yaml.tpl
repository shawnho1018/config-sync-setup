apiVersion: v1
kind: ServiceAccount
metadata:
  name: %%NAMESPACE%%-ksa
  annotations:
    iam.gke.io/gcp-service-account: cnrm-system-%%NAMESPACE%%@%%PROJECT_ID%%.iam.gserviceaccount.com
