# ROOT_REPO/namespaces/NAMESPACE/repo-sync.yaml
apiVersion: configsync.gke.io/v1alpha1
kind: RepoSync
metadata:
  name: repo-sync
  namespace: %%NAMESPACE%%
spec:
  git:
   repo: "%%REPOSITORY%%"
   revision: "HEAD"
   branch: "master"
   dir: "cluster-config/%%NAMESPACE%%"
   auth: "token"
   secretRef:
     name: "git-creds"