# Overview

This is a brief description of the practical uses of cluster selectors. This is not meant to be a replacement for the official documentation

# Cluster Setup

Initial setup of the Configuration Management tool requires **each cluster** to setup a `ConfigurationManagement` object similar to this:

```yaml
apiVersion: configmanagement.gke.io/v1
kind: ConfigManagement
metadata:
  name: config-management
  namespace: config-management-system
spec:
  # Unique across all clusters using the same ConfigSync repo. Name is used in Cluster Selectors
  clusterName: unique-cluster-name
  # If Anthos customer, enable Policy Management
  policyController:
    enabled: true
```

>NOTE: The `spec.clusterName` assigns a "name" to the cluster for the ConfigManagement tool to anchor configuration. This value MUST be unique within the group of clusters using the same ACM configuration repository.

# Repo Cluster Configuration

Within the configuration cluster's repository, the namespace for `/clusterregistry` is used to contain configuration for the cluster selector feature. There are two types of files/objects this folder should contain:

1. `Cluster` definitions that are designed to be a logical representation of a single cluster and contain labels to describe the purpose, contents, usage, location and other relevant information about the cluster
1. `ClusterSelector` definitions that are used to group together `Cluster`s for use by the KRM resources indicated with the annotation `configmanagement.gke.io/cluster-selector: <cluster-selector-name>`

## Cluster Objects

`Cluster` KRM objects are a Custom Resource Definition (CRD) installed with the ACM/Config-Sync installation. `Cluster` KRM objects describe the cluster using labels and are anchored back to a physical cluster using the `metadata.name` field. The `metadata.name` MUST match a single name of a cluster within the group of clusters using the same configuration repository [#cluster-setup](see cluster setup)
